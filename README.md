# FrogMuffins OS

A real-time operating system for the arm920t architecture written in C (with no standard libraries as they were unavailiable for said architecture) coupled with a train controller.

This project was written for [cs452](https://www.student.cs.uwaterloo.ca/~cs452/) in collaboration with [Barbara Macdonald](https://github.com/BarbaraEMac/frogmuffins/blob/master/todo.txt)


## Features

- a micro-kernel with
    - a very *slick* context switch (written in arm assembly)
    - multi-threaded
    - protected memmory access
- memmory allocation
- fast re-implementation of basic data types (such as `string`, `buffer`) and math functions
- a shell
- drivers for
    - clock interrups
    - serial port communication
- a real-time train controller that intelligently prevents train collisions


## Requirements

- A `C` compiler capable of compiling for the `arm920t` architecture.
- That's it, lol.


## Building

```bash
make
```
